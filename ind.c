#define F_CPU 8000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ENC_A (1 << 5)
#define ENC_B (1 << 0)
#define RESET (1 << 1)

const float coef = 0.1;

const unsigned char font[] = { 0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x40 };

unsigned short displayedNumber;
unsigned char pointmask;
short counter;

unsigned char getAt(short number, unsigned char index)
{
	if (number < 0)
	{
		number = -number;
		if (index == 3)
			return font[10];	//"-"
	}
	while (index--)
		number /= 10;
	return font[number % 10];
}

ISR(PCINT_vect)
{
	if (PINB & ENC_A)
	{
		if (PINA & ENC_B)
			counter++;
		else
			counter--;
	}
}

void light()
{
	for (unsigned char i = 0; i < 4; i++)
	{
		PORTD = getAt(displayedNumber, 3 - i);
		if (pointmask & (1 << (3 - i)))
			PORTB |= 16;
		else
			PORTB &=~ 16;
		PORTB &= ~(1 << i);
		_delay_ms(1);
		PORTB |= 0x0f;
	}
}

int main (void)
{
	DDRB = 0x1f;
	PORTB = 0xe0;
	DDRD = 0xff;
	PORTD = 0xff;

	PORTA |= ENC_B;
	PORTA |= RESET;
	PORTB |= ENC_A;

	PCMSK = ENC_A;
	GIMSK = (1 << PCIE);
	sei();

	displayedNumber = 0;
	pointmask = 2;
	while(1)
	{
		light();
		if (!(PINA & RESET))
			counter = 0;
		displayedNumber = roundf(counter * coef);
	}

    return 0;
}
